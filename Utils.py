from Model import Stack, Variable, Operation
from google.appengine.api import memcache


def getUndoStack():
    stack = Stack.query(Stack.name == 'undoStack').get()
    if stack is None:
        stack = Stack(name='undoStack', itemIds=[])
        stack.put()
    return stack


def getRedoStack():
    stack = Stack.query(Stack.name == 'redoStack').get()
    if stack is None:
        stack = Stack(name='redoStack', itemIds=[])
        stack.put()
    return stack


def get_variable(variableName):
    variable = Variable.query(Variable.id == variableName).get()
    return variable


def get_name_from_request(self):
    variableName = self.request.get('name', "not found")
    return variableName


def saveOperation( variableName, variablePreviousValue, variableValue):
    lastOperationID = memcache.get('lastOperationID')
    if lastOperationID is None:
        memcache.add('lastOperationID', 1)
        lastOperationID = 1
    operationId = lastOperationID + 1
    operation = Operation(id=operationId, variableName=variableName, value=variableValue,
                          previousValue=variablePreviousValue)
    operation.put()
    memcache.incr('lastOperationID', 1)
    return operationId