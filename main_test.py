import webtest

import main


def test_set():
    app = webtest.TestApp(main.app)

    response = app.get('/set')

    assert response.status_int == 200
    assert response.body == 'THIS IS SET'