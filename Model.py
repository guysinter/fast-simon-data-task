from google.appengine.ext import ndb


class Variable(ndb.Model):
    id = ndb.StringProperty()
    value = ndb.StringProperty()


class Operation(ndb.Model):
    id = ndb.IntegerProperty()
    variableName = ndb.StringProperty()
    value = ndb.StringProperty()
    previousValue = ndb.StringProperty()


class Stack(ndb.Model):
    name = ndb.StringProperty()
    itemIds = ndb.IntegerProperty(repeated=True)
