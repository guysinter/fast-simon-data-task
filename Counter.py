from google.appengine.api import memcache


def incrementCounter(variableValue):
    memcache.incr(variableValue, delta=1, namespace=None, initial_value=0)


def decrementCounter(variableValue):
    memcache.decr(variableValue)


def getCount(variableValue):
    return memcache.get(variableValue)
