import webapp2

from google.appengine.ext import ndb
from google.appengine.api import memcache

from Counter import incrementCounter, decrementCounter , getCount
from Model import Variable, Operation, Stack
from Utils import getUndoStack, getRedoStack, get_variable, get_name_from_request, saveOperation


class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, Fast simon !')


class GetHandler(webapp2.RequestHandler):
    def get(self):
        variableName = get_name_from_request(self)
        variable = get_variable(variableName)
        self.response.headers['Content-Type'] = 'text/plain'
        if variable is not None:
            self.response.write(variable.value)
        else:
            self.response.write('None')


class SetHandler(webapp2.RequestHandler):

    def get(self):
        variableName = get_name_from_request(self)
        variableValue = self.request.get('value', "not found")
        previousVariableValue = 'None'

        variable = get_variable(variableName)
        if variable is not None:
            previousVariableValue = variable.value
            decrementCounter(previousVariableValue)
            variable.value = variableValue
        else:
            variable = Variable(id=variableName, value=variableValue)
        variable.put()
        incrementCounter(variableValue)

        operationId = saveOperation(variableName, previousVariableValue, variableValue)

        undoStack = getUndoStack()
        undoStack.itemIds.append(operationId)
        undoStack.put()

        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write(variableName + ' = ' + variableValue)


class UnsetHandler(webapp2.RequestHandler):
    def get(self):
        variableName = get_name_from_request(self)
        variable = get_variable(variableName)
        if variable is not None:
            variablePreviousValue = variable.value
            decrementCounter(variablePreviousValue)
            variable.value = 'None'
            variable.put()
            operationId = saveOperation(variableName, variablePreviousValue, 'None')
            undoStack = getUndoStack()
            undoStack.itemIds.append(operationId)
            undoStack.put()
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write(variableName + ' = None')




class NumEqualToHandler(webapp2.RequestHandler):
    def get(self):
        variableValue = self.request.get('value', "not found")
        count = getCount(variableValue)
        self.response.headers['Content-Type'] = 'text/plain'
        if count is None:
            self.response.write('0')
        else:
            self.response.write(count)


class UndoHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        undoStack = getUndoStack()
        if len(undoStack.itemIds) == 0:
            self.response.write('NO COMMANDS')
        else:
            operationID = undoStack.itemIds.pop()
            undoStack.put()
            operation = Operation.query(Operation.id == operationID).get()
            if operation is not None:
                variable = get_variable(operation.variableName)
                variable.value = operation.previousValue
                variable.put()
                redoStack = getRedoStack()
                redoStack.itemIds.append(operation.id)
                redoStack.put()
                if operation.previousValue != 'None':
                    incrementCounter(operation.previousValue)
                if operation.value != 'None':
                    decrementCounter(operation.value)
                self.response.write(variable.id + ' = ' + variable.value)
            else:
                self.response.write('NO COMMANDS')


class RedoHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        redoStack = getRedoStack()
        if len(redoStack.itemIds) == 0:
            self.response.write('NO COMMANDS')
        else:
            operationID = redoStack.itemIds.pop()
            redoStack.put()
            operation = Operation.query(Operation.id == operationID).get()
            if operation is not None:
                variable = get_variable(operation.variableName)
                variable.value = operation.value
                variable.put()
                undoStack = getUndoStack()
                undoStack.itemIds.append(operation.id)
                undoStack.put()
                if operation.previousValue != 'None':
                    decrementCounter(operation.previousValue)
                if operation.value != 'None':
                    incrementCounter(operation.value)
                self.response.write(variable.id + ' = ' + variable.value)
            else:
                self.response.write('NO COMMANDS')


class EndHandler(webapp2.RequestHandler):
    def get(self):
        ndb.delete_multi(
            Variable.query().fetch(keys_only=True)
        )
        ndb.delete_multi(
            Stack.query().fetch(keys_only=True)
        )
        ndb.delete_multi(
            Operation.query().fetch(keys_only=True)
        )
        memcache.flush_all()
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('CLEANED')


app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/set', SetHandler),
    ('/get', GetHandler),
    ('/unset', UnsetHandler),
    ('/numequalto', NumEqualToHandler),
    ('/undo', UndoHandler),
    ('/redo', RedoHandler),
    ('/end', EndHandler)
], debug=False)
